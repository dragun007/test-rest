<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property int $id
 * @property string|null $name
 * @property float|null $rate
 */
class Currency extends \yii\db\ActiveRecord
{

	const SCENARIO_VIEW=1;
	/**
	 *
	 */
	const UPDATE_URL='http://www.cbr.ru/scripts/XML_daily.asp';


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rate'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'rate' => 'Rate',
        ];
    }

	/**
	 * {@inheritdoc}
	 */
	public function fields()
	{
		if($this->scenario===self::SCENARIO_VIEW)
			$params=[
				'rate',
			];
		else
			$params=parent::fields();
		return $params;
	}

	/**
	 * set attributes by SimpleXMLElement  object
	 *
	 * @param object $xmlObject
	 */
	public function setAttributesByXmlObject($xmlObject)
    {
    	$nominal=null;
    	$value=null;
    	$xmlVars=get_object_vars ($xmlObject);

	    foreach ($xmlVars as $fieldName=>$childValue)
	    {

		    	if($fieldName=='Name')
				    $this->name=$childValue;
			    elseif ($fieldName=='Nominal')
				    $nominal=$childValue;
			    elseif ($fieldName=='Value')
				    $value=floatval(str_replace(',', '.',$childValue));


	    }
	    if(isset($nominal)&&isset($value))
	    	$this->rate=$value/$nominal;

    }
}
