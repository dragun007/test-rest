<?php
namespace app\controllers;

use app\models\Currency;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;
use Yii;

class CurrencyController extends ActiveController
{
    public $modelClass = 'app\models\Currency';


    public function beforeAction( $action ) {
	    if(Yii::$app->controller->action->id=='view')
	    \yii\base\Event::on(Currency::class, Currency::EVENT_AFTER_FIND, function ($event) {
		    $event->sender->scenario = Currency::SCENARIO_VIEW;
	    });
	    return parent::beforeAction( $action );
    }

	public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];
        return $behaviors;
    }


}
